# -*- coding: utf-8 -*-

nbr1 = int(input("Entrez un nombre: "))
nbr2 = int(input("Entrez un second nombre: "))

if nbr1 > 0 and nbr2 > 0:
    print("Le produit des deux nombres est positif.")
elif nbr1 < 0 and nbr2 > 0:
    print("Le produit des deux nombres est négatif.")
elif nbr1 > 0 and nbr2 < 0:
    print("Le produit des deux nombres est négatif.")
else:
    print("Le produit des deux nombres est nulle.")