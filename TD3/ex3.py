# -*- coding: utf-8 -*-

hour = int(input("Entrez vos heures de travaille: "))

if hour<=39:
    print("Vous n'avez pas fait d'heures supplémentaires.")
elif 39<hour<=44:
    hsup = hour-(39)
    bonus = (10.03*hsup)*1.5
    print("Vous avez travaillé", hsup, "heures en plus et avait été payé","%.2f" % bonus, "€ en plus.")
elif 44<hour<=49:
    hsup = hour-(39)
    bonus = (10.03*hsup)*1.75
    print("Vous avez travaillé", hsup, "heures en plus et avait été payé","%.2f" % bonus, "€ en plus.")
else:
    hsup = hour-(39)
    bonus = (10.03*hsup)*2
    print("Vous avez travaillé", hsup, "heures en plus et avait été payé","%.2f" % bonus, "€ en plus.")