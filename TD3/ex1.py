# -*- coding: utf-8 -*-

start = int(input("Entrez le début de l'intervalle: "))
end = int(input("Entrez la fin de l'intervalle: "))
value = int(input("Entrez un nombre: "))

if value<=end and value>=start:
    print("Bravo ! Le nombre est compris dans l'intervalle !")
else:
    print("Dommage ! Le nombre n'est pas compris dans l'intervalle !")