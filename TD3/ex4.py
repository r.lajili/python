# -*- coding: utf-8 -*-

note = float(input("Entrez votre note sur 20: "))

if note < 12:
    print("Vous n'avez pas de mention. Cheh !")
elif 12<=note<14:
    print("Vous avez la mention 'Assez Bien'. Bien joué !")
elif 14<=note<16:
    print("Vous avez la mention 'Bien'. Bravo !")
else:
    print("Vous avez la mention 'Très Bien'. Félicitations !")