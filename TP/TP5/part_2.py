class PersonnelIUT :
    def __init__(self,p,n):
        self.prenom=p
        self.nom=n
        self.Bureau=''
    def __str__(self):
        return self.prenom+" "+self.nom+" travail a l'IUT"
    def __eq__(self,other) :
        return (self.prenom,self.nom)==(other.prenom,other.nom)
    def emmenage(self,other):
        self.Bureau=other
        


class Prof(PersonnelIUT) :
    """ classe fille avec 5 attributs 'nom';'prenom';'moduleEnseignes';'statut',bureau
    """
    def __init__(self, p, n,s='MCF') :
        super().__init__(p,n)
        self.modulesEnseignes=list()
        self.statut=s
        
    def __str__(self) :
        s=self.prenom+" "+self.nom+" travail a l'IUT en tant que prof des modules "
        for a in self.modulesEnseignes:
            s=s+a+', '
        return s
    def setStatut(self,other):
         self.statut=other

    def enseigne(self,other):
        self.modulesEnseignes.append(other)
        
    def enseignePlusRien(self):
        self.modulesEnseignes=list()
        
    def enseignePlus(self,other):
        self.modulesEnseignes.remove(other)
        
        
class Administratif(PersonnelIUT) :
    def __init__(self,p,n):
        super().__init__(p,n)
        self.poste=''
    def __str__(self):
        return self.prenom+" "+self.nom+" travail a l'IUT en tant que "+self.poste
    def setPoste(self,other):
        self.poste=other
        
        
class EquipePedagogique:
    def __init__(self,l):
        self.libelle=l
        self.monEquipe=list()
    def __str__(self):
        s = self.libelle + ' : '
        for p in self.monEquipe:
            s += ( '\n' + str(p) )
        return s
    
    def rajoute(self,p):
        self.monEquipe.append(p)
        
    def suprimme(self,p):
        self.monEquipe.remove(p)

if __name__ == '__main__':
    equ = EquipePedagogique('Equipe Pedagogique IUT')
    print(equ)
    p1 = Prof("Jean-Paul","Jamont")
    p1.enseigne("M1101")
    p1.enseigne("M3101")
    p1.emmenage("C102")
    equ.rajoute(p1)
    print(equ)
    p2 = Prof("Fred","Michel","CDI")
    p2.emmenage("C103")
    equ.rajoute(p2)
    print(equ)
    p2.enseigne("M1101")
    p3 = Prof("Chris","Duc","PRAG")
    p3.emmenage("C106")
    equ.rajoute(p3)
    print(equ)
    a1 = Administratif("Christine","Galdon")
    a1.emmenage("C115")
    equ.rajoute(a1)
    p1.enseignePlusRien()
    print(equ)
    a1.setPoste('secrétaire R&T')
    print(equ)
    a=input("fini : appuyez sur une touche!")