# -*- coding:Utf-8 -*-
"""module Date pour TP 1 : archi rudimentaire !
"""
# fonction pour gérer correctement le mois de Février
def estBissextile(millesime) :
    return millesime % 400 == 0 or (millesime % 4 == 0 and millesime % 100 != 0)

# Classe Date
class Date :
    """
    sert à gérer de façon simpliste une date d'anniversaire
    """
    
    # méthodes spéciales
    def __init__(self,jours,mois,annee):
        """ Constructeur de la classe Date : initialise les attributs avec
            ce qu'il reçoit en parametres, supposés entiers
        """
        self.jours =jours
        self.mois =mois
        self.annee =annee
    def __str__(self):
            return str(self.jours)+" "+str(self.mois)+" "+str(self.annee)
    def __gt__(self, other):
        a=self.jours-other.jours
        b=self.mois-other.mois
        c=self.annee-other.annee
        if c<0 :
            return False
        if c==0:
            if b<0:
                return False
            if b==0:
                if a<0:
                    return False
                if a==0:
                    return False
        else:
            return True
    def __eq__(self,other):
        if self.sameYear(other) and self.sameDay(other) and self.sameDay(other):
            return True
        else:
            return False
    def lendemain(self):
        a=self.jours
        b=self.mois
        c=self.annee
        if a==31 and b in [1,3,5,7,8,10,12]:
            a==1
            b=b+1
        elif a==30 and b not in [1,3,5,7,8,10,12]:
            a=1
            b=b+1
            
        elif a==28 and b==2 and estBissextile(c)==False:
            a=1
            b=3
        elif a==29 and b==2 and estBissextile(c):
            a=1
            b=3
        elif b>12:
            b=1
            c=c+1
        else:
            a=a+1
        return str(a)+" "+str(b)+" "+str(c)
        
    def veille(self):
        a=self.jours
        b=self.mois
        c=self.annee
        if a==1 and b in [1,5,7,10,12]:
            a=30
            b=b-1
        elif a==1 and b in [2,4,6,8,9,11]:
            a=31
            b=b-1
        elif a==1 and b==3 and estBissextile(c)==True:
            a=29
            b=2
        elif a==1 and b==3 and estBissextile(c)==False:
            a=28
            b=2
        elif b<1:
            a=a=31
            b=b=12
            c=c=c-1
        else:
            a=a-1
        return str(a)+" "+str(b)+" "+str(c)
    def sameYear(self,other):
        return self.annee==other.annee
    def sameMonth(self,other):
        return self.mois==other.mois
    def sameDay(self,other):
        return self.jours==other.jours
    
        
            
        
            






        
