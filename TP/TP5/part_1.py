class Prof :
    """ classe élémentaire avec 2 attributs 'nom' et 'prenom' de type 'chaîne'
    """
    def __init__(self, p, n) :
        self.prenom = p
        self.nom=n
        self.modulesEnseignes=list()
        
    def __str__(self) :
        if self.modulesEnseignes==list():
            return self.nom+" "+self.prenom+" flemmard qui fait rien"
        else:    
            s=self.nom+" "+self.prenom+" modules enseignés: "
            for a in self.modulesEnseignes:
                s=s+a+', '
            return s
        
    
    def __eq__(self,other) :
        return (self.prenom,self.nom)==(other.prenom,other.nom)

    def enseigne(self,other):
        self.modulesEnseignes.append(other)
    def enseignePlusRien(self):
        self.modulesEnseignes=list()
    def enseignePlus(self,other):
        self.modulesEnseignes.remove(other)

class GroupeDeProfs :
    """ Classe de collection ayant comme attributs :
        - mesProfs : une liste (de "Prof" !!)
        - libelle : un nom d'équipe
    """
    # Constructeur de la classe
    def __init__(self, nom) :
        # déclaration et initialisation des attributs "libelle" et equipe
        self.libelle = nom
        self.mesProfs = list()    # ou self.mesProfs = [] : initialisée vide!
           
    # la méthode suivante surcharge celle de "object"
    def __str__(self) :
        s = self.libelle + ' : '
        for p in self.mesProfs:
            s += ( '\n' + str(p) )
        return s
    
    # méthodes "spécifiques" : A COMPLETER
    def rajoute(self,prof) :    # rajoute un élément à la liste (on espère un prof?)
        self.mesProfs.append(prof)

if __name__ == '__main__':
    mesProfs = GroupeDeProfs('Equipe Pedagogique R&T')
    print(mesProfs)
    p1 = Prof("Jean-Paul","Jamont")
    p1.enseigne('M2101')
    p1.enseigne('M2103')
    mesProfs.rajoute(p1)
    print(mesProfs)
    p2 = Prof("chris","Duc")
    mesProfs.rajoute(p2)
    print(mesProfs)
    a=input("fini : appuyez sur une touche!")
   
    
   
    
    