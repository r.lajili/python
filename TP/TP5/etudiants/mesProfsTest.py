# -*- coding: utf-8 -*-

class Prof :
    """ classe élémentaire avec 2 attributs 'nom' et 'prenom' de type 'chaîne'
    """
    # Constructeur de la classe : A COMPLETER !!!
    def __init__(self, p, n) :
        self.prenom = p     # déclaration et initialisation de l'attribut "prenom"
        self.nom = n        # à rajouter : déclaration et initialisation de l'attribut "nom"
        self.modulesEnseignes=list()
        
    # les 3 méthodes suivantes surchargent celle de "object"
    # __str__ : A COMPLETER !!! : __str__ doit rendre une chaîne :
    # la chaîne attendue est une simple succession
    # du "prenom nom" (avec un espace entre les deux)
    def __str__(self, p, n) :
        s= self.prenom, " ", self.nom
        return s
    
    def __eq__(self,other) :
        return (self.prenom,self.nom)==(other.prenom,other.nom)

    # Ecrire ici les méthodes "métiers" (enseigne, etc...)
    
    def enseigne(self,m):
        self.modulesEnseignes.append(m)
    
    def enseignePlusRien(self):
        self.modulesEnseignes.clear()

class GroupeDeProfs :
    """ Classe de collection ayant comme attributs :
        - mesProfs : une liste (de "Prof" !!)
        - libelle : un nom d'équipe
    """
    # Constructeur de la classe
    def __init__(self, nom) :
        # déclaration et initialisation des attributs "libelle" et equipe
        self.libelle = nom
        self.mesProfs = list()    # ou self.mesProfs = [] : initialisée vide!
           
    # la méthode suivante surcharge celle de "object"
    def __str__(self) :
        s = self.libelle + ' : '
        for p in self.mesProfs:
            s += ( '\n' + str(p) )
        return s
    
    # méthodes "spécifiques" : A COMPLETER
    def rajoute(self,prof) :    # rajoute un élément à la liste (on espère un prof?)
        pass

if __name__ == '__main__':
    mesProfs = GroupeDeProfs('Equipe Pedagogique R&T')
    print(mesProfs)
    p1 = Prof("Jean-Paul","Jamont")
    p1.enseigne('M2101')
    p1.enseigne('M2103')
    mesProfs.rajoute(p1)
    print(mesProfs)
    p2 = Prof("chris","Duc")
    mesProfs.rajoute(p2)
    print(mesProfs)
    a=input("fini : appuyez sur une touche!")
    
    
