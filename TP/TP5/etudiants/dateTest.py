# -*- coding: utf-8 -*-
from date import Date

# les deux méthodes ci-dessous devraient être dans la classe Date, mais pour
# simplifier je les ai mises ici
def getDate(yMin,yMax) :
    invite = "Saisir une date entre le 1° janvier %d et le 31 décembre %d \nAnnée : " % (yMin, yMax)
    annee = int( input(invite) )
    mois = int( input("Mois (entre 1 et 12) : ") )
    jour = int ( input("Jour (entre 1 et 28,29,30 ou 31): ") )
    return Date(jour,mois,annee)
        
def genereDate(yMin,yMax) :
    from random import randint
    annee = randint(yMin, yMax)
    mois = randint(1, 12)
    jour = randint(1, 28) # hé hé, pour simplifier le code
    return Date(jour,mois,annee)

# le code à analyser et faire fonctionner débute ici
if __name__ == '__main__':
    d1 = Date(1,1,1900)
    print(d1)
    lendemain1 = d1.lendemain() # rend une Date !!!
    print("le lendemain du jour ",d1, " est le ", lendemain1)
    
    d2 = Date(31,12,2999)
    print(d2)
    veille2 = d2.veille()       # rend une Date !!!
    print("le jour avant le %s est le %s" % (d2,veille2))
    
    d3 = genereDate(1900,2999)
    invite = """ Il faut trouver une date entre le "%s" et le %s """ % (d1,d2)
    print("(debug) %s" % d3)
    
    fini = False
    while not fini :
        d = getDate(1900,2999)
        print("Vous avez saisi la date %s, dont la veille est le %s et le lendemain le %s" % (d, d.veille(), d.lendemain()))
        if d == d3 :
            print("Vous avez trouvé!")
            fini = True
        else :
            if d3 > d :
                print("la date à trouver est postérieure!")
            else :
                print("la date à trouver est antérieure!")
            if d3.sameYear(d) :     # rend un booléen !!!
                print("l'année est bonne!")
            if d3.sameMonth(d) :    # rend un booléen !!!
                print("le mois est bon!")
            if d3.sameDay(d) :      # rend un booléen !!!
                print("le jour est bon!")
            
    print("Bye !")
                    
        
