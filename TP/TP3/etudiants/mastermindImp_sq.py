# -*- coding: utf-8 -*-
# La fonction "submit" compare la proposition l'opérateur ("propal") avec
#   celle à trouver ("combiOrdi") et rend un "tuple" de 2 entiers dont :
#   le premier élément est le nombre de "bien placés"
#   le second élément est le nombre de "mal placés"
def submit(propal,combiOrdi):
    bienPlaces = 0  
    malPlaces = 0
    # A compléter ....


    # ---- r est le résultat du traitement ...
    r = (bienPlaces,malPlaces)
    assert bienPlaces + malPlaces <= 5
    return r
        
if __name__ == '__main__' :
    print("******* Mastermind ***********")
    combiOrdi=['black','black','black','black','black']
    listeCouleurs =['green','red','white','purple','blue','yellow']
    from random import randint
    for i in range(5) :
        combiOrdi[i] = listeCouleurs[randint(0,5)]
    print(combiOrdi)    # pour pouvoir tricher
    
    fini = False
    nbEssai = 0
    while not fini :
        nbEssai = nbEssai+1
        print("choix parmi : ", listeCouleurs)
        propalJoueur = eval(input("Votre proposition : "))
        r = submit(propalJoueur,combiOrdi)
        if r[0] == 5 :
            print("Vous avez GAGNE !! (en {} coups)".format(nbEssai))
            fini = True
        else :
            print("Bien Placés : {}, Mal Placés : {}".format(r[0],r[1]))
            if nbEssai == 10 :
                print("Vous avez perdu !!")
                print(" la combinaison à trouver était :")
                print(combiOrdi)
                fini = True
            else :
                print("Il vous reste {} essai(s)".format(10-nbEssai))
                
    print("Bye!")
