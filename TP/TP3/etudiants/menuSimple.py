"""Récupération élémentaire d'exception...
"""

import sys

def lireEntier(a,b) :
    invite = "Entrez un nombre entier entre {0} et {1}, {2} pour sortir : ".format(a,b,a-1)
    i = int(input(invite))
    return i
    
def menuSimple() :
    fini = False
    while not fini :
        try:    
            i = lireEntier(1,9)
        except ValueError:
            print("Veuillez entrer un nombre entier !")
            return menuSimple() # Ré-éxécute menuSimple()
        except KeyboardInterrupt:
            a = str(input("Êtes-vous sur de vouloir quitter o/n ? "))
            if a == "o":
                sys.exit()
            else:
                return menuSimple()
        if i == 0 :
            fini = True
        else :
            print ("Nb saisi : {}, son double est : {}".format(i,2*i))
    print ("Bye !")
    
if __name__ == '__main__':
    menuSimple()
