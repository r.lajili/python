# -*- coding: utf-8 -*-

def submit(propal,combiOrdi):
    bienPlaces = 0  
    malPlaces = 0
    for i in range(0,5):
        if propal[i]==combiOrdi[i]:
            bienPlaces=bienPlaces+1
        else:
            malPlaces=malPlaces+1
    r = (bienPlaces,malPlaces)
    assert bienPlaces + malPlaces <= 5
    return r
        
if __name__ == '__main__' :
    print("*********** Mastermind ***********")
    combiOrdi=['black','black','black','black','black']
    listeCouleurs =['green','red','white','purple','blue','yellow']
    from random import randint
    for i in range(5) :
        combiOrdi[i] = listeCouleurs[randint(0,5)]
    print(combiOrdi)
    
    fini = False
    nbEssai = 0
    while not fini :
        nbEssai = nbEssai+1
        print("Choix parmi : ", listeCouleurs)
        propalJoueur = eval(input("Votre proposition : "))
        r = submit(propalJoueur,combiOrdi)
        if r[0] == 5 :
            print("Vous avez GAGNE !! (en {} coups)".format(nbEssai))
            fini = True
        else :
            print("Bien Placés : {}, Mal Placés : {}".format(r[0],r[1]))
            if nbEssai == 10 :
                print("Vous avez perdu !!")
                print("La combinaison à trouver était :")
                print(combiOrdi)
                fini = True
            else :
                print("Il vous reste {} essai(s)".format(10-nbEssai))
                
    print("Bye!")
