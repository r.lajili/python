
def euro_2_autre(veuro, monn):
    
    try : 
        if monn == "USD" :
            veuro *= 1.12
            return veuro 
        
        elif monn == "GBP" :
            veuro *= 0.897
            return veuro 
        
        elif monn == "JPY" :
            veuro *= 115
            return veuro 
        
        elif monn == "MAD" :
            veuro *= 10.82
            return veuro 
        
        elif monn == "THB" :
            veuro *= 38.45
            return veuro 
        
        elif monn == "CHF" :
            veuro *= 1.086
            return veuro 
        
    except KeyError :
        print("Devise non valide")
        

def TableConversion(veuro, monn):
        
    if monn == "USD" :
        for i in range(0, veuro+1 , 10) :
            r = euro_2_autre(i, "USD")
            print(i , " euro(s) = ", r , " USD")
            
    elif monn =="GBP" :
        for i in range(0 , veuro+1 , 10) :
            r = euro_2_autre(i, "GBP")
            print(i , " euro(s) = ", r , " GBP")
            
    elif monn =="JPY" :
        for i in range(0 , veuro+1 , 10) :
            r = euro_2_autre(i, "JPY")
            print(i , " euro(s) = ", r , " JPY")
            
    elif monn =="MAD" :
        for i in range(0 , veuro+1 , 10) :
            r = euro_2_autre(i, "MAD")
            print(i , " euro(s) = ", r , " MAD")
            
    elif monn =="THB" :
        for i in range(0 , veuro+1 , 10) :
            r = euro_2_autre(i, "THB")
            print(i , " euro(s) = ", r , " THB")
            
    elif monn =="CHF" :
        for i in range(0 , veuro+1 , 10) :
            r = euro_2_autre(i, "CHF")
            print(i , " euro(s) = ", r , " CHF")

veuro = int(input("Entrez la valeur à convertir : "))
monn = str(input("Unité de conversion : USD, GBP, JPY, MAD, THB, CHF ==> "))

TableConversion( veuro , monn )

