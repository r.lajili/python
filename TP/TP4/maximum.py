# -*- coding: utf-8 -*-

import random

maListe = random.sample(range(-100,100),6) # Liste aléatoire comprise entre -100 et 100 de 6 valeurs
print ("Liste aléatoire:",maListe) # Affichage
max = maListe[0] # Désignation d'une valeur max qui sera utiliser pour afficher celle de la liste

for x in maListe:
    if x > max:
        max = x
print ("La valeur max de la liste est:",max)