# -*- coding: utf-8 -*-

inventaire = {'pommes':430, 'bananes':312, 'oranges':274,
'poires':137}
del inventaire['pommes']
print (inventaire)
print (inventaire.keys()) # que donne la méthode keys() ?
print (inventaire.values()) # que donne la méthode values() ?
print (inventaire.items()) # que donne la méthode items() ?