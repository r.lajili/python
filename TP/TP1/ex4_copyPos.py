# -*- coding: utf-8 -*-

import random # importation du module random

MaListe = random.sample(range(-100,100), 6) # création d'une liste aléatoire de 6 termes allant de -100 à 100

ListePos=[] # Initialisation d'une liste vierge
print("Liste aléatoire: ",MaListe) # Affichage de la liste aléatoire
for i in MaListe: 
    if i>0: # Sélection des valeurs positives dans la liste aléatoire
        ListePos=ListePos+[i] # Incrémentation des valeurs positives dans la liste vierge
print("Liste comprenant uniquement les nombres positives: ", ListePos) # Affichage de la nouvelle liste