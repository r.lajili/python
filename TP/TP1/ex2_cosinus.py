# -*- coding: utf-8 -*-

from math import cos # Importation du module consinus issue de la bibliothèque math

angle = int(input("Entrez un angle: ")) # Récupération d'une valeur entière

if (0 <= angle <= 360): # Si angle est compris entre 0 et 360
     n = 0 # Initialisation de n à 0
     while n<=angle: # Tant que n est inférieur ou égal à angle
         cosn = cos(n) # éxécution du module cosinus
         print("cos",n,"=", cosn) # Affichage/Résultat
         n += 10 # Incrémentation de 10 de la variable n
else: # Sinon
    print("Erreur !") # Erreur