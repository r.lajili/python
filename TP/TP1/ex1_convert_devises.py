# -*- coding: utf-8 -*-

nbr = int(input("Entrez une somme entre 1€ et 100€: "))
n = 0 # Initialisation d'une variable n

if (1 <= nbr <= 100): # Si nbr est compris entre 1 et 100
    while (n <= nbr): # Tant que n est inférieur ou égal à nbr
        c = n*1.12 # Produit (conversion)
        print(n,"euro(s) =", "%.2f" % c, "$") # Affichage du résultat de la conversion
        n += 1 # Variable n incrémenté de 1
else: # Sinon
    print("Erreur !") # Erreur