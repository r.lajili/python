# -*- coding: utf-8 -*-
from math import pi

n = float(input("Entrez un nombre entre 0 et 2pi: ")) # Initialisation d'une variable flottante

c = str(input("Degrés (d) ou Grades (g): ")) # Initialisation d'un caractère (d ou g)

if c == "g": # Si c = g
    g = n*(200/pi) # Conversion radian vers grades
    print("Un angle de", n, "radians vaut", g, "grades") # Affichage du résultat
else: 
    if c == "d": # Si c = d
        d = n*(180/pi) # Conversion radian vers degrés
        print("Un angle de", n, "radians vaut", "%.2f" % d, "grades") # Affichage du résultat
    else: 
        print("Erreur !") # Affichage si erreur
