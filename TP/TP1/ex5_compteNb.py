# -*- coding: utf-8 -*-

import string # Importation du module string
Dico=dict.fromkeys(string.ascii_lowercase,0) # Extraction des caractères dans une variable

chaine=input("Entrez une chaîne de caractère: ")
liste=list(chaine) # Liste des caractères
for i in Dico:
    z=liste.count(i) # Comptabilisation de chaque caractère dans la liste
    if z!=0:
        print(i,"=",z) # Affichage du nombre de chaque caractère
