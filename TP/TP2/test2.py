# -*- coding: utf-8 -*-

maListe = [1, 3, 2] # Les éléments de la liste de base sont dans le désordre
maListe.sort() # Tri croissant des éléments de la liste
print (maListe) # Affichage de la liste triée
maListe.reverse() # Inversion de l'ordre des éléments de la liste (Tri décroissant)
print (maListe) # Affichage de la liste dans l'ordre inversé
print (maListe.reverse()) # Aucun affichage car maListe.reverse() n'est pas une liste existante