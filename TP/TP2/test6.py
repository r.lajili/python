# -*- coding: utf-8 -*-

d1 = {'a':1, 'b':2}
d2 = d1
d1['b'] = 3
print (d1)
print (d2)
d3 = d1.copy()
print (d3)
d1['b'] = 4
print (d1)
print (d2)
print (d3)
print (id(d1), id(d2), id(d3))
# ce qui précède doit vous rappeler ce que vous avez vu
# au dessus avec les listes !
print ('a' in d1)
print ('z' in d1)
print (d1.keys())
print (d1.values())
print (d1.items())
d3.clear()
print (d3)
d3 = {'a':9, 't':3}
d1.update(d3)
print (d1)
