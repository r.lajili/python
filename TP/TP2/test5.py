# -*- coding: utf-8 -*-

ch1 = 'tutu'
print(len(ch1)) # Affiche la longueur de ch1
ch2 = ch1.capitalize() # Copie ce que contient ch1 vers ch2 en passant le premier terme en majuscule
print (ch2) # Affiche ch2
print (ch1) # Affiche ch1
print (ch1.islower()) # Affiche si oui on non ch1 est en minuscule
print (ch2.islower()) # Affiche si oui on non ch2 est en majuscule
ch3 = ch1.replace('tu', 'ton') # Remplace 'tu' dans ch1 par 'ton' et copie vers ch3
print (ch3) # Affiche ch3
print (ch1.count('tu')) # Affiche le nombre de 'tu' dans ch1
ch4 = ' -*- '.join([ch1,ch2,ch3]) # Rejoint le contenu de ch1, ch2 et ch3 dans ch4 en les espacant par ' -*- '
print (ch4) # Affiche ch4
ch5 = 'AaBbCc'.lower() # Passe tous les caractères de ch5 en minuscules
print (ch5) # Affiche ch5
ch6 = 'AaBbCc'.upper() # Passe tous les caractères de ch6 en majuscules
print (ch6) # Affiche ch6
ch7 = ' coucou '.strip() # Retire les espaces avant et après 'coucou'
print (ch7) # Affiche ch7
ch8 = ' 1 toto 3 12.5 '.split() # Sépare chaque terme (Affichage similaire a une liste)
print (ch8) # Affiche ch8
