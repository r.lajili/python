﻿# ATTENTION : ci-dessous, vous devez éventuellement remplacer "tp4_cor" par le (ou les) noms du/des
#   fichiers qui contiennent les fonctions des exercices 2, 3 et 4.
# Vous devez également bien orthographier les-dites fonctions (respectez la spéc!)
# ensuite allez voir dans le "__main__" (à la fin du fichier.
# Lorsque le programme fonctionne, écrivez la fonction "mps2mph" (réciproque de "mph2mps") et décommentez
# les lignes nécessaires.
from tp4_cor import factorielle,volumeSphere,mph2mps
from tp4_cor import factorielle,volumeSphere,mph2mps,mps2mph
from tkinter import *
                
_fT = ('Courrier', 14) # tuple pour avoir une police plus grosse

def blackHole(x) :
    return None

class ValeurNum :    
    def __init__(self,nom=None, default = None, noFloat=False, mini = None, maxi = None, chkFct=None, unit=None ):
        self.nom = nom
        self.noFloat = noFloat
        self.chkFct = chkFct
        self.unit = unit
        self.val = default
        self.min = mini
        self.max = maxi

class GraphicalNumValue(Frame) :    
    def __init__(self,valnum, writeable, parent=None, rtnFct=None):
        Frame.__init__(self, parent)
        self.parent = parent
        self.valeur = valnum
        self.rtnFct = rtnFct
        if self.valeur.noFloat :
            self.contenu = IntVar()
        else :
            self.contenu = DoubleVar()
        if self.valeur.nom is not None :
            Label(parent, text = self.valeur.nom + " : ").pack(side=LEFT, expand=YES, fill=BOTH)
        self.setValue(self.valeur.val)
        self.entree = Entry(parent,textvariable=self.contenu)
        self.entree.pack(side=LEFT, expand=YES, fill=BOTH)
        if writeable :
            self.entree.bind('<Key-Return>',self.eventHandler)
        else :
            self.entree.config(state=DISABLED)
        if self.valeur.unit is not None :
            Label(parent, text = self.valeur.unit).pack(side=LEFT, expand=YES, fill=BOTH)

    def getValue(self):
        try :
            if self.valeur.noFloat :
                typs = "entier"
            else :
                typs = "réel"
            self.valeur.val = self.contenu.get()
        except (TypeError,ValueError) as e :
            messagebox.showerror("Erreur","Ce n'est pas un {}!".format(typs))
            self.valeur.val = None
            self.contenu.set('')
        else :
            if self.valeur.min is not None and self.valeur.val < self.valeur.min or\
               self.valeur.max is not None and self.valeur.val > self.valeur.max :
                messagebox.showerror("Erreur","{} non valide".format(typs.capitalize()))
                self.valeur.val = None
                self.contenu.set('')
        return self.valeur.val

    def setValue(self,nb):
        self.valeur.val = nb
        if nb is None :
            s = ''
        elif self.valeur.noFloat :     
            s = str (nb)
        else :
            s = "{:.6f}".format(nb)
        self.contenu.set(s)

    def eventHandler(self,event):
        self.rtnFct()
        
class GraphicalConverteur :    
    def __init__(self, titre, vg, vd , cnvFctGD = None, cnvFctDG = None , parent=None ): 
        frm = Frame(parent)
        frm.pack(side = TOP, expand=YES, fill=BOTH)
        Label(frm, text = titre + " : ", font = _fT).pack(side=LEFT, expand=YES, fill=BOTH)
        self.cnvFctGD = cnvFctGD
        self.cnvFctDG = cnvFctDG
        self.valGauche = vg
        self.ValeurGraphGauche = GraphicalNumValue(self.valGauche, True, frm, self.gauche2Droite)
        Button(frm,text = '->', command = self.gauche2Droite, font = _fT ).pack(side=LEFT, expand=YES, fill=BOTH)
        self.valDroite = vd
        cvtDG = False
        fctDG = None
        if self.cnvFctDG is not None :
            cvtDG = True
            Button(frm,text = '<-', command = self.droite2Gauche , font = _fT).pack(side=LEFT, expand=YES, fill=BOTH)
            fctDG = self.droite2Gauche
        self.ValeurGraphDroite = GraphicalNumValue(self.valDroite, cvtDG, frm, fctDG)

    def gauche2Droite(self) :
        x = self.ValeurGraphGauche.getValue()
        if x is not None :
            y = self.cnvFctGD(x)
            self.ValeurGraphDroite.setValue(y)

    def droite2Gauche(self) :
        x = self.ValeurGraphDroite.getValue()
        if x is not None :
            y = self.cnvFctDG(x)
            self.ValeurGraphGauche.setValue(y)
        
if __name__ == '__main__':
    root = Tk()
    GraphicalConverteur("Volume d'une sphère", ValeurNum(nom="rayon",unit="mètre",default=0, mini=0),\
               ValeurNum(nom="volume",unit="mètre-cube",default=0),\
               cnvFctGD=volumeSphere, parent=root)
    GraphicalConverteur("Factorielle", ValeurNum(nom="n", default=0, noFloat=True, mini=0),\
               ValeurNum(nom="n!", default=1, noFloat=True),\
               cnvFctGD=factorielle,parent=root)
    GraphicalConverteur("Convertisseur de vitesse",ValeurNum(unit="miles/heures", default=0, mini=0),\
               ValeurNum(unit="mètres/secondes", default=0, mini=0),\
               cnvFctGD=mph2mps,cnvFctDG=mps2mph, parent=root)
## Si vous n'avez pas encore écrit "mps2mph", il faut garder la ligne ci-dessus. Sinon, remplacez-là par
##  celle ci-dessous! (il faut aussi l'importer, revoir tout début du fichier!
##               cnvFctGD=mph2mps,cnvFctDG=mps2mph, parent=root)
    root.mainloop()
