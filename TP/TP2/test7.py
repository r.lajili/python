# -*- coding: utf-8 -*-

def doubler(x):
    """Retourne le double de <n>.""" #  Chaîne de documentation
    return x*2
print (doubler(7)) # ... un entier
print (doubler(7.2)) # ... un flottant...
print (doubler("bla")) # ... une chaîne...
print (doubler(["a"])) # ... une liste...
print (doubler({1:'bleu', 2:'rouge', 3:'vert'})) # On ne sait pas faire !
