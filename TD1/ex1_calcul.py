# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 14:36:20 2019

@author: lajil
"""

a, b = int(input("a vaut: ")), int(input("b vaut: "))

somme = a + b
print("somme de a et b :", somme)

diff = a - b
print("différence de a et b :", diff)

prod = a * b
print("produit de a et b :", prod)

rapport = (a / b)
print("rapport a/b :", "%.2f" % rapport)
